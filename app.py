#!/usr/bin/env python3.6

from flask import Flask,jsonify,request,render_template,redirect

from pyindico import nextmeet
from os import getenv

app = Flask(__name__)

@app.route('/next/<category>')
def next_meeting(category):
    nm,date = nextmeet.main(category,getenv("CONFIG_PATH"))    
    output = {'body' : nm, 'date' : date}
    return jsonify(output)

if __name__ == "__main__":
    app.run()
