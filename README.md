# pyIndico - Indico API access from Python.

PyIndico is a Python library and command-line interface for accessing
[Indico](https://indico.cern.ch), CERN's service for scheduling meetings
and events.

The Indico API (which you can read about [here](https://docs.getindico.io/en/stable/http_api/))
has lots of cool features! This program/library is very primitive right
now; it knows how to look up the next meeting in a category but not
much more at the moment. More features might be added in the future!

## Setup

PyIndico is a Python 3 package. I haven't uploaded it to the Python Package
Index yet but for now you can do the following to clone and install it:

```
git clone [URL]
cd pyindico
python3 setup.py sdist
pip3 install --user dist/*.tar.gz
```

To use this, you need to grab your Indico API keys. They can be found under
"My Preferences" and then the "HTTP API" tab. "Token" is your "API Key" and
"Secret" is your "Secret Key". (Look at the Indico API documentation for more
information on what these mean). You can stick these in a configuration
file of the following form:

```
$ cat ~/.config/pyindico/indico.conf
[keys]
api = ${API_KEY}
secret = ${SECRET_KEY}
```

PyIndico needs to be able to read this file to start up.

## Usage

PyIndico installs a Python script called ```indico-next ```, which can
tell you the next meeting in a given category, provided you know the number.
This is the number that appears in a URL of the form ```$ indico-next https://indico.cern.ch/category/${ID}/```.
For example, the ATLAS UPenn Meetings category has index '1566'.

```
indico-next 1566
Next Meeting: Weekly Group Meeting: https://indico.cern.ch/event/737080/ in 4-S-020 (CERN)
```

### Defining Aliases

Remembering numbers is a little annoying. So, you can add an "alias" section
to the configuration file. Try adding these lines to the bottom of
the file we created above:

```
[alias]
upenn = 1566
```

Now you should be able to run the following command:

```
indico-next upenn
```

And get the same output.

### Defining Indico Instance

This script/library can be used with non-CERN Indico instances, by adding
the following section to your configuration file:

```
[global]
url = https://indico.cern.ch
```

## Limitations

This software is in an 'early alpha'. In particular, there are some
limitations you should be aware of...

1. It doesn't know about timezones. Or rather, it does, but it doesn't
take them into account when computing the "next meeting".

2. It doesn't create a template configuration file for you automatically;
it will simply complain that one isn't there. You must copy the above
template and fill it in manually.

3. It can't currently combine categories (tell you the next meeting
in categories A and B), though it would be pretty simple to add.

## License

PyIndico is made available under the MIT license.
