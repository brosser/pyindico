#!/usr/bin/env python3

import argparse
import datetime
import os
import sys

import dateutil
import dateutil.tz
import pytz

from . import config
from . import events

def main(category, config_path, limit=10, timetable=False):

    # Look for configuration file.
    if not os.path.exists(config_path):
        print("Error: configuration directory '" + config_path + "' does not exist. You must make it manually for now.")
        sys.exit(1)
    config_file = os.path.join(config_path, "indico.conf")
    if not os.path.exists(config_file):
        print("Error: configuration file '" + config_file + "' does not exist. You must make it manually for now.")
        sys.exit(1)

    # Load API keys, list of category aliases from configuration file.
    apikey, secretkey, baseurl, aliases = config.load_config(config_file)

    # First see if it's an integer.
    try:
        category = int(category)
    except ValueError:
        try:
            category = int(aliases[category])
        except:
            print("Error: category alias '" + category + "' not defined in indico.conf.")
            sys.exit(1)

    # Look up the category.
    contents = events.get_category(category, limit=limit, baseurl=baseurl, api_key=apikey, secret_key=secretkey, from_param='today')
    cat_events = events.parse_category(contents, category, baseurl, apikey, secretkey)

    # Find the next event that hasn't happened yet!
    curtime = datetime.datetime.now()
    curtz = dateutil.tz.gettz()
    curtime = curtime.replace(tzinfo=curtz)

    next_event = None
    for event in cat_events:
        if event.start >= curtime:
            if next_event is None:
                next_event = event
            elif next_event.start >= event.start:
                next_event = event

    if next_event is not None:
        event_time = next_event.start.astimezone(pytz.timezone('GMT'))
        next_event.load_timetable(baseurl, apikey, secretkey)
        message = "Next Meeting: " + str(next_event)
        if timetable:
            for contribution in next_event.contributions:
                message += "\n" + str(contribution)
        return message, event_time
    else:
        return "No meetings scheduled for the future!", ''

def entrypoint():
    default_config_path = config.get_default_config()

    parser = argparse.ArgumentParser(description='Look up the next meeting in an Indico category.')
    parser.add_argument('category', metavar='CATEGORY', help="Category number (or alias) to check.")
    parser.add_argument('-c', '--config', dest='config', default=default_config_path, help="Configuration file directory.")
    parser.add_argument('-l', '--limit', dest='limit', default=10, type=int, help="Number of events to retrieve per category.")
    parser.add_argument('-t', '--timetable', dest='timetable', action="store_true", help="Show the timetable for the next event.")

    args = parser.parse_args()

    print(main(args.category, args.config, args.limit, args.timetable)[0])

if __name__ == '__main__':
    entrypoint()
