# Indico Event/Category classes and parsing.

import datetime

# Depend on dateutil for timezone handling.
import dateutil
import dateutil.tz

from . import api
from . import contributions

class IndicoCategory:

    def __init__(self, name, link, index):
        self.name = name
        self.link = link
        self.index = index

class IndicoEvent:

    def __init__(self, name, start, end, link, location, room, tz, event_id):
        self.name = name
        self.start = start
        self.end = end
        self.link = link
        self.location = location
        self.room = room

        # Assume the start and end timezones are the same?
        self.tz = tz

        self.event_id = event_id

        # Store list of contributions
        self.contributions = []

    def __str__(self):
        msg = self.name
        msg += " on " + self.start.strftime("%c %Z")

        if self.room != '':
            msg += " in " + self.room
            if self.location != '':
                msg += " (" + self.location + ")"

        msg += ": " + self.link
        return msg

    def __repr__(self):
        return self.__str__()

    def load_timetable(self, baseurl, api_key, secret_key):
        raw_timetable = contributions.get_timetable(self.event_id, baseurl, api_key, secret_key)
        self.contributions = contributions.parse_timetable(raw_timetable, self.event_id)

def get_category(index, limit=10, baseurl="https://indico.cern.ch", api_key=None, secret_key=None, from_param=None, to_param=None):
    """	Retrieves JSON for a category."""
    path = '/export/categ/' + str(index) + '.json'

    params = [('limit', limit)]

    # Define a "from" and a "to" param.
    if from_param is not None:
        params.append(('from', from_param))
    if to_param is not None:
        params.append(('to', to_param))

    indico_url = api.build_indico_request(path, params, api_key, secret_key)
    contents = api.get_json_from_url(baseurl, indico_url)
    return contents

def parse_category(contents, index, baseurl, api_key=None, secret_key=None):
    """	Given contents of an Indico category, parse it."""
    num_events = contents["count"]
    events = contents["results"]

    # Get the category metadata first.
    parents = contents["additionalInfo"]["eventCategories"]
    try:
        parent_path = parents[0]['path']
    except IndexError:
        return []
    category_name = None
    category_link = None
    #for path in parent_path:
    #	if path['id'] == str(index):
    #		category_name = path['name']
    #		category_link = path['url']
    #		break

    # Don't know that we need to do anything with this metadata for now.
    #if category_name is not None:
    #	category = IndicoCategory(category_name, index, category_link)

    parsed_events = []

    # Loop through the number of returned events.
    for i in range(num_events):
        title = events[i]["title"]
        start_date = events[i]["startDate"]
        end_date = events[i]["endDate"]
        link = events[i]["url"]
        event_id = events[i]["id"]

        # Location fields; there are two?
        location = events[i]["location"]
        room = events[i]["room"]

        start, start_tz = parse_date(start_date)
        end, end_tz = parse_date(end_date)

        # Create an IndicoEvent class.
        event = IndicoEvent(title, start, end, link, location, room, start_tz, event_id)

        # Populate the timetable! Note: this requires the API keys.
        # Let's disable this here for now, and do it on request?
        #raw_timetable = contributions.get_timetable(event.event_id, baseurl, api_key, secret_key)
        #contribs = contributions.parse_timetable(raw_timetable, event.event_id)
        #for contrib in contribs:
        #    event.contributions.append(contrib)

        # Append to list of events.
        parsed_events.append(event)

    return parsed_events

def parse_date(date_json):
    """	Given a JSON date retrieved from Indico, return a datetime object."""
    ymd = date_json['date']
    tzstr = date_json['tz']
    hmd = date_json['time']

    date = datetime.datetime.strptime(ymd + ' ' + hmd, "%Y-%m-%d %H:%M:%S")

    # Use dateutil to make this datetime object 'tz aware'.
    retrieved_tz = dateutil.tz.gettz(tzstr)
    date = date.replace(tzinfo=retrieved_tz)

    return date, tzstr
