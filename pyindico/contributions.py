# Contributions library for pyIndico.

from . import api

import pprint

class IndicoPresenter:

    def __init__(self, name, email, affiliation):
        self.name = name
        self.email = email
        self.affiliation = affiliation

    def __str__(self):
        if self.email is not None:
            return ("%s (%s)" % (self.name, self.email))
        else:
            return ("%s" % self.name)

    def __repr__(self):
        return self.__str__()

class IndicoContribution:

    def __init__(self, event_id, title):
        self.event_id = event_id
        self.title = title

        self.presenters = []

    def __str__(self):
        msg = "Contribution: '" + self.title + "' from: "
        for presenter in self.presenters:
            msg += str(presenter) + ", "
        msg = msg[:-2]
        return msg

    def __repr__(self):
        return self.__str__()

    def add_presenter(self, presenter):
        self.presenters.append(presenter)

def get_timetable(event_id, baseurl, api_key=None, secret_key=None):
    path = '/export/timetable/' + str(event_id) + '.json'
    params = []
    indico_url = api.build_indico_request(path, params, api_key, secret_key)
    contents = api.get_json_from_url(baseurl, indico_url)
    return contents

def parse_timetable(contents, event_id):
    # This should be fixed to handle multiple dates.
    dict_timetable = contents["results"][event_id].values()
    contributions = []
    try:
        timetable = list(dict_timetable)[0]
    except:
        return contributions
    for raw_contrib in timetable.values():

        title = raw_contrib["title"]
        contrib = IndicoContribution(event_id, title)

        presenters = raw_contrib["presenters"]
        for presenter in presenters:
            first_name = presenter["firstName"]
            family_name = presenter["familyName"]
            full_name = first_name.rstrip() + " " + family_name.rstrip()

            affiliation = presenter["affiliation"]
            try:
                email = presenter["email"]
            except KeyError:
                email = None

            author = IndicoPresenter(full_name, email, affiliation)
            contrib.add_presenter(author)

        contributions.append(contrib)
    return contributions
