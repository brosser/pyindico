# Configuration file loader and handler.

import configparser
import os

def get_default_config():
    return os.path.expanduser('~/.config/pyindico/')

def load_config(filename):
    """ Loads configuration file found at 'filename'."""
    parser = configparser.ConfigParser()
    aliases = {}

    # Make sure the file exists.
    filename = os.path.abspath(filename)
    if not os.path.exists(filename):
        return None, None, aliases

    # Load it.
    parser.read(filename)

    # Look for a 'keys' section.
    try:
        apikey = parser['keys']['api']
        secretkey = parser['keys']['secret'].encode()
    except KeyError:
        apikey = None
        secretkey = None

    # See if the configuration file defined a non-CERN indico instance.
    try:
        baseurl = parser['global']['url']
    except:
        baseurl = "https://indico.cern.ch"

    if 'alias' in parser.sections():
        for alias, index in parser.items('alias'):
            aliases[alias] = index

    # This is clumsy; we should return some kind of configuration object.
    return apikey, secretkey, baseurl, aliases
