# Routines for interacting with the Indico API.

import hashlib
import hmac
import json
import time
import urllib

# This is dumb, and almost worse than 'import urllib2'.
import urllib.parse
import urllib.request

def build_indico_request(path, params, api_key=None, secret_key=None, only_public=False, persistent=False):
    """	This function taken from the Indico API documentation."""
    items = list(params)
    if api_key:
        items.append(('apikey', api_key))
    if only_public:
        items.append(('onlypublic', 'yes'))
    if secret_key:
        if not persistent:
            items.append(('timestamp', str(int(time.time()))))
        items = sorted(items, key=lambda x: x[0].lower())
        url = '%s?%s' % (path, urllib.parse.urlencode(items))
        signature = hmac.new(secret_key, url.encode(), hashlib.sha1).hexdigest()
        items.append(('signature', signature))
    if not items:
        return path
    return '%s?%s' % (path, urllib.parse.urlencode(items))

def get_json_from_url(base_url, indico_url):
    indico_url = base_url + indico_url
    contents = None
    #print(indico_url)
    with urllib.request.urlopen(indico_url) as indico_site:
        contents = indico_site.read()
        json_contents = json.loads(contents.decode())

    return json_contents
